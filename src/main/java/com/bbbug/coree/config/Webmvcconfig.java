package com.bbbug.coree.config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.yeauty.standard.ServerEndpointExporter;

@Configuration
public class Webmvcconfig implements WebMvcConfigurer {

@Value("${web.upload-path}")
String mypath;
@Override
public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/uploads/**").addResourceLocations("file:"+mypath);
    registry.addResourceHandler("/**").addResourceLocations("file:"+mypath);
}
}
