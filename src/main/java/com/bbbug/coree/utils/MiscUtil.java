package com.bbbug.coree.utils;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.jwt.JWT;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MiscUtil {
    private static final long EXPIRE_TIME = 45 * 60 * 1000;//默认45分钟
    private static final String TOKEN_SECRET = "BBBUG";
    public static String createToken(String userId) {
            Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
            String token = JWT.create()
                    .setPayload("userid", userId)
                    .setKey(TOKEN_SECRET.getBytes(StandardCharsets.UTF_8))
                    .setExpiresAt(date)
                    .sign();
            return token;
    }
    public static String createToken(String userId,Integer second) {

        Date date = new Date(System.currentTimeMillis() + second*1000);
        String token = JWT.create()
                .setPayload("userid", userId)
                .setKey(TOKEN_SECRET.getBytes(StandardCharsets.UTF_8))
                .setExpiresAt(date)
                .sign();
        return token;

    }
    public static String verifyToken(String token){
        try {
            JWT jwt = JWT.of(token);
            //System.out.println(new Date(jwt.getPayloads().getLong("exp")));
            return jwt.getPayloads().getStr("userid");
        }catch (Exception e){
            return null;
        }

    }
    public static String getres(String key)  {
        key=key.replace(" ","&nbsp");
        String randomString= RandomUtil.randomNumbers(8);
        String url = "http://bd.kuwo.cn/api/www/search/searchMusicBykeyWord?key="+key+"&pn=1&rn=30";
        Map<String,String> header=new HashMap<>();
        header.put("Referer","http://bd.kuwo.cn");
        header.put("csrf",randomString);
        header.put("Cookie","kw_token="+randomString);
        String s="";
        try {
            s=HttpUtil.createGet(url).addHeaders(header).execute().body();
        } catch (Exception e) {

        }
        return s.replace("&nbsp"," ");
    }
    public static String getLRC(String key)  {
        String url = "http://m.kuwo.cn/newh5/singles/songinfoandlrc?musicId="+key;
        String randomString= RandomUtil.randomNumbers(8);
        Map<String, String> hashMap = new HashMap();
        hashMap.put("Referer","http://bd.kuwo.cn");
        hashMap.put("csrf",randomString);
        hashMap.put("Cookie","kw_token="+randomString);
        String s="";
        try {
            s = HttpUtil.createGet(url).addHeaders(hashMap).execute().body();
        } catch (Exception e) {

        }
        s=s.replace("&nbsp"," ");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data",JSON.parseObject(s).getJSONObject("data").getJSONArray("lrclist"));
        jsonObject.put("code",200);
        jsonObject.put("msg","获取成功");

        return jsonObject.toJSONString();
    }
    private final static TemplateEngine templateEngine = new TemplateEngine();

    /**
     * 使用 Thymeleaf 渲染 HTML
     * @param template  HTML模板
     * @param params 参数
     * @return  渲染后的HTML
     */
    public static String render(String template, Map<String, Object> params){
        Context context = new Context();
        context.setVariables(params);
        return templateEngine.process(template, context);
    }

}
