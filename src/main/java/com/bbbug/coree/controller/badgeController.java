package com.bbbug.coree.controller;

import cn.hutool.core.codec.Base64;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bbbug.coree.entity.Roominfo;
import com.bbbug.coree.service.RooominfoService;
import com.bbbug.coree.utils.SpringUtils;
import com.bbbug.coree.websocketservice.core;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

@CrossOrigin(origins ="${myweb-path}" )
@Controller
public class badgeController {
    @Value("${myweb-path}")
    String mywebpath;
    RedisTemplate redisTemplate= SpringUtils.getBean("redistemp");
    @Autowired
    private RooominfoService rooominfoService;

    @RequestMapping(value = "api/badge/{room_id}",produces="application/json;charset=UTF-8" )
    String badge(@PathVariable("room_id") String roomid, HttpServletResponse response,HashMap<String, String> mp ){
        //HashMap<String, String> mp = new HashMap<>();
        if(core.CHATMAP.get(roomid)==null){
            roomid="888";
        }
        BoundListOperations boundListOperations = redisTemplate.boundListOps(roomid);
        if(boundListOperations.size()==0){
        }else{
            JSONObject song = JSON.parseObject(boundListOperations.index(0).toString());
            JSONObject song1 = song.getJSONObject("song");
            JSONObject user = song.getJSONObject("user");
            mp.put("song_pic","data:image/png;base64,"+Base64.encode(HttpUtil.createGet(song1.getString("pic")).execute().bodyStream()));
            mp.put("song_singer",song1.getString("singer"));
            mp.put("song_name",song1.getString("name"));
            mp.put("user_name",user.getString("user_name"));
            Roominfo res = rooominfoService.queryById(Integer.parseInt(roomid));
            mp.put("room_name",res.getRoom_name());
        }
        mp.put("roomid",roomid);
        mp.put("mywebpath",mywebpath+"/");

        return "h1";
    }
    @RequestMapping(value = "/api/badge/player/{room_id}",produces = {"text/html"})
    ModelAndView getplayerBadge(@PathVariable("room_id") String roomid){
        if(core.CHATMAP.get(roomid)==null){
            roomid="888";
        }
       // mp.put("roomid",roomid);

        return new ModelAndView("player");
    }

}
