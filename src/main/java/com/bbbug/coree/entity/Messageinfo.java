package com.bbbug.coree.entity;

import lombok.Data;

import java.io.Serializable;
@Data

public class Messageinfo implements Serializable {
    private static final long serialVersionUID = 240926990378090534L;
    private String  message_id;
    private Integer  message_user;
    private String  message_type;
    private String  message_where;
    private Integer  message_to;
    private String  message_content;
    private Integer  message_status;
    private Long  message_createtime;
    private Long  message_updatetime;
}
