package com.bbbug.coree.entity;

import lombok.Data;

import java.io.Serializable;
@Data
public class Roominfo implements Serializable {
    private static final long serialVersionUID = -68893498001005483L;
    /**
     * 房间id
     */
    private Integer  room_id;
    /**
     * 所属人id
     */
    private Integer  room_user;
    /**
     * 在线人数
     */
    private Integer  room_online;
    /**
     * 房间分数
     */
    private Integer  room_score;
    /**
     * 房间名称
     */
    private String  room_name;
    /**
     * 类型
     */
    private Integer  room_type;
    /**
     * 公告
     */
    private String  room_notice;
    /**
     * 1开启房主点播
     */
    private Integer  room_addsong;
    /**
     * 是否全员禁言
     */
    private Integer  room_sendmsg;
    /**
     * 自动点歌
     */
    private Integer  room_robot;
    /**
     * 图一乐
     */
    private String  room_huya;
    /**
     * 封禁原因
     */
    private String  room_reason;
    /**
     * 是否封禁
     */
    private Integer  room_status;
    /**
     * 是否需要密码
     */
    private Integer  room_public;
    /**
     * 密码
     */
    private String  room_pwd;
    /**
     * 房间所属人
     */
    private Integer admin;
    /**
     * 背景图片
     */
    private String room_background;
    //点歌数量最大上限
    private Integer room_addcount;
    //点歌cd
    private Integer room_addsongcd;
    //顶歌间隔
    private Integer room_pushsongcd;
    //投票切歌
    private Integer room_votepass;
    //是否单曲循环
    private Integer room_playone;
    //日顶次数
    private Integer room_pushdaycount;

}
