package com.bbbug.coree.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Lrcinfo implements Serializable {
    private static final long serialVersionUID = 2965501777518355613L;
    private Integer id;
    private String lrc;
    private Integer count;
}
