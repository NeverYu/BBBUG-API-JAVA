package com.bbbug.coree.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

//{"code":200,"msg":"Hello World!","data":{"time":1624416829076}}
@Data
public class ReturnData implements Serializable {
    private static final long serialVersionUID = -65536L;
    Integer code;
    String msg;
    Object data;
    public ReturnData(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
