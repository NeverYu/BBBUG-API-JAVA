package com.bbbug.coree.mapper;

import com.bbbug.coree.entity.Songinfo;
import com.bbbug.coree.entity.SongSubinfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
@Mapper
public interface SongMapper {
    @Select("select * from songinfo order by hot DESC LIMIT 0,30")
    List<Songinfo> queryByLimit();
    @Select("select * from songinfo where mid=#{mid}")
    Songinfo queryByMid(Integer mid);
    @Insert("insert into songinfo (mid, name, pic, singer, length,hot) value (#{mid},#{name},#{pic},#{singer},#{length},#{hot})")
    Boolean insert(Songinfo songinfo);
    @Update("update songinfo set hot=hot+1 where mid=#{mid}")
    Boolean updateSong(Integer mid);
    @Select("select songinfo.mid, name, pic, singer, length,played from songinfo,subscribeinfo where subscribeinfo.user_id=#{user_id} and subscribeinfo.mid=songinfo.mid")
    List<SongSubinfo> queryByUserid(Integer user_id);



}
