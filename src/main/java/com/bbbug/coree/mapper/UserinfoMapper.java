package com.bbbug.coree.mapper;

import com.bbbug.coree.entity.Userinfo;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface UserinfoMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param userId 主键
     * @return 实例对象
     */
    @Select("select user_id,user_app,user_img,user_sex,user_vip,user_head,user_name,user_remark,user_account,user_touchtip,user_remark,appname from userinfo where user_id = #{userId}")
    Userinfo queryById(Integer userId);
    @Select("select * from userinfo where user_account = #{userAccount}")
    Userinfo queryByAccount(String userAccount);
    @Select("select * from userinfo where user_account=#{account} and user_passwd = #{password}")
    Userinfo queryByBoth(String account,String password);
    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Select("select user_id, user_sex, user_account, user_touchtip, user_name, user_head, user_remark, user_song, user_img, user_gamesongscore, user_app, user_device, appname, user_passwd from userinfo limit #{offset}, #{limit}")
    List<Userinfo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param userinfo 实例对象
     * @return 实例对象
     */
    @Insert("insert into userinfo (user_account,user_passwd,user_name,user_sex,user_head,user_touchtip,appname) values ( #{user_account},#{user_passwd},#{user_name},#{user_sex},#{user_head},#{user_touchtip},#{appname}) ")
    Boolean insert(Userinfo userinfo);

    /**
     * 修改数据
     *
     * @param userinfo 实例对象
     * @return 实例对象
     */
    @Update("update userinfo set user_head=#{user_head},user_name=#{user_name},user_passwd=#{user_passwd},user_sex=#{user_sex},user_remark=#{user_remark},user_touchtip=#{user_touchtip} where user_id=#{user_id}")
    Boolean update(Userinfo userinfo);

    /**
     * 通过主键删除数据
     *
     * @param userId 主键
     * @return 是否成功
     */
    @Delete("delete from userinfo where user_id = #{userId}")
    boolean deleteById(Integer userId);
    @Select("select user_id from userinfo where appname=#{third}")
    Userinfo queryBythrid(String third);
}
