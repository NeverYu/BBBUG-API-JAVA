/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : 513_sixth

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 01/08/2021 10:21:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lrcinfo
-- ----------------------------
DROP TABLE IF EXISTS `lrcinfo`;
CREATE TABLE `lrcinfo`  (
  `id` int(11) NOT NULL,
  `lrc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `count` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lrcinfo
-- ----------------------------

-- ----------------------------
-- Table structure for messageinfo
-- ----------------------------
DROP TABLE IF EXISTS `messageinfo`;
CREATE TABLE `messageinfo`  (
  `message_id` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `message_user` int(11) NULL DEFAULT NULL,
  `message_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `message_where` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `message_to` int(11) NULL DEFAULT NULL,
  `message_content` varchar(2550) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `message_status` int(255) NULL DEFAULT NULL,
  `message_createtime` bigint(20) NULL DEFAULT NULL,
  `message_updatetime` bigint(20) NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of messageinfo
-- ----------------------------

-- ----------------------------
-- Table structure for roominfo
-- ----------------------------
DROP TABLE IF EXISTS `roominfo`;
CREATE TABLE `roominfo`  (
  `room_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '房间id',
  `room_user` int(11) NULL DEFAULT NULL COMMENT '所属人id',
  `room_online` int(11) NULL DEFAULT NULL COMMENT '在线人数',
  `room_score` int(11) NULL DEFAULT NULL COMMENT '房间分数',
  `room_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '房间名称',
  `room_type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `room_notice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公告',
  `room_addsong` int(11) NULL DEFAULT NULL COMMENT '1开启房主点播',
  `room_sendmsg` int(11) NULL DEFAULT NULL COMMENT '是否全员禁言',
  `room_robot` int(255) NULL DEFAULT NULL COMMENT '自动点歌',
  `room_huya` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图一乐',
  `room_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封禁原因',
  `room_status` int(11) NULL DEFAULT NULL COMMENT '是否封禁',
  `room_public` int(255) NULL DEFAULT NULL COMMENT '是否需要密码',
  `room_pwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `admin` int(11) NULL DEFAULT NULL,
  `room_background` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '背景',
  `room_addcount` int(255) NULL DEFAULT NULL COMMENT '点歌数量最大上限',
  `room_addsongcd` int(255) NULL DEFAULT NULL COMMENT '点歌cd',
  `room_pushsongcd` int(255) NULL DEFAULT NULL COMMENT '顶歌间隔',
  `room_votepass` int(255) NULL DEFAULT NULL COMMENT '投票切歌',
  `room_playone` int(255) NULL DEFAULT NULL COMMENT '是否单曲循环',
  PRIMARY KEY (`room_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 90166 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roominfo
-- ----------------------------
INSERT INTO `roominfo` VALUES (888, 1, 1, 1, 'ts1**', 4, '欢迎！!', 0, 0, 0, '0', '0', 0, 0, '', 1, '', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for songinfo
-- ----------------------------
DROP TABLE IF EXISTS `songinfo`;
CREATE TABLE `songinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` bigint(20) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `singer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `length` int(11) NULL DEFAULT NULL,
  `hot` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `mid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1261 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of songinfo
-- ----------------------------

-- ----------------------------
-- Table structure for subscribeinfo
-- ----------------------------
DROP TABLE IF EXISTS `subscribeinfo`;
CREATE TABLE `subscribeinfo`  (
  `mid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `played` int(11) NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of subscribeinfo
-- ----------------------------

-- ----------------------------
-- Table structure for userinfo
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo`  (
  `user_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `user_sex` int(11) NULL DEFAULT NULL COMMENT '1男0女',
  `user_account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮箱',
  `user_touchtip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '戳一戳',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '昵称',
  `user_head` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像URL',
  `user_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户签名',
  `user_song` int(11) NULL DEFAULT NULL COMMENT '点歌数量',
  `user_img` int(11) NULL DEFAULT NULL COMMENT '发图数量',
  `user_gamesongscore` int(11) NULL DEFAULT NULL COMMENT '猜歌得分',
  `user_app` int(11) NULL DEFAULT NULL COMMENT '是否为移动端',
  `user_device` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登陆设备',
  `appname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'apc看点',
  `user_passwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `user_vip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of userinfo
-- ----------------------------
INSERT INTO `userinfo` VALUES (1, 1, 'admin', '的键盘', 'Robot', 'new/images/nohead.png', '132', NULL, NULL, NULL, NULL, 'Windows', 'Vue', '123gzx', 'robot');

SET FOREIGN_KEY_CHECKS = 1;
