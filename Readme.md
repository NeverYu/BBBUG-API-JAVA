# BBBUG聊天室 JAVA API端

<p align="center">
<a href="https://bbbug.com" target="_blank"><img src="http://www.gzxtest.xyz:8080/api/badge/888"/></a>
</p>

### 介绍

此仓库为BBBUG项目后端JAVA API部分，其他客户端代码请查看组织下的对应仓库[组织仓库](https://gitee.com/bbbug_com)

若追求项目完整 请移步至[ChatAPI](https://gitee.com/bbbug_com/ChatAPI)

### 免责声明

平台音乐和视频直播流数据来源于第三方网站，仅供学习交流使用，请勿用于商业用途。

### 技术架构

相较于原项目后端API，使用Springboot实现后端接口，并集成Netty实现的Websocket，

### 项目文档（自动生成的）

[javadoc文档](https://apidoc.gitee.com/kiseryota/sixth_work/)

### 快速体验
[传送门](http://www.gzxtest.xyz)


### 如何自己GKD？

1.导入数据库文件 513_sixth.sql

2.部署并修改前端的appid以及前端的链接参数 [BBBUG前端传送门](https://gitee.com/bbbug_com/ChatWEB)
快速gkd前端参数如下
````
    // API后端地址
    apiUrl: "http://127.0.0.1:8080/",
    // 静态文件地址 如不使用CDN 请保持跟上面一致
    staticUrl: "http://127.0.0.1:8080/",
    // Websocket连接地址
    wssUrl: "ws://127.0.0.1:8081/ws",
````

3.git clone也好，download也行，copy也可以，把源代码导入idea即可快速gkd

4.把imgupload.7z解压在 {web.upload-path}下（默认E盘根目录）以加载静态文件

5.若静态资源文件加载不出来，可能是因为前端的http2https所导致



### 其他

是大三课题口胡的东西，毕竟不会写前端QWQ

腾讯三方未申请下来Appid 相对应的三方登陆暂时不可用（虽然逻辑写好了但没有测试）


### 计划
```
登陆&注册(√)

发送消息(√)

听歌&听歌同步&点歌(√)

切换房间(√)

显示个人信息(√)

显示在线人数(√)

房间信息修改（√）

禁言(嘉宾部分未完成)

上传歌曲(×)

数据库和原作统一(×)


```